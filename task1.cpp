#include <iostream>
#include "CustomerCounter.h"
using namespace std;

int main(){
  int amount;
  cout<< "Enter the max amount of customers: ";
  cin>>amount;
  auto c = CustomerCounter(amount);
  cout<< "\nInitial amount: ";
  c.display();
  cout<<"\nAfter adding 10:";
  c.add(10);
  c.display();
  cout<<"\nAfter adding 10 more: ";
  c.add(10);
  c.display();
  cout<<"\nAfter subtracting 7: ";
  c.subtract(7);
  c.display();
}
