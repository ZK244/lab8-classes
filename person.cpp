#include "Person.h"
#include <iostream>
#include <algorithm>
#include <vector>
using namespace std;

Person::Person(string name)
  : name(name), friends(vector<Person*>()){};

void Person::befriend(Person* person){
  this->friends.push_back(person);
}

void Person::unfriend(Person* person){
  auto to_unfriend = find(this->friends.begin(), this->friends.end(),person);
  if (to_unfriend != this->friends.end()){
    this-> friends.erase(to_unfriend);
  }
}

void Person::display(){
  cout<<"Person: "<< "\nName: "<< this->name<<"\nFriends: ";
  for (auto f : this->friends){
    cout<< f->name<<" ";
  }
  cout<<endl;
}
