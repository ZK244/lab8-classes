#include <iostream>
#include <vector>
#include "Shapes.h"

int main() {
    Square s1 = Square(3);
    Square s2 = Square(11);

    Circle c1 = Circle(45);
    Circle c2 = Circle(8.5);

    Rectangle r1 = Rectangle(65.4, 3);
    Rectangle r2 = Rectangle(28, 55.7);

    std::vector<Shape*> shapes = {&s1, &s2, &c1, &c2, &r1, &r2};

    for (auto shape : shapes) {
        std::cout << "[Shape]\n" << "\n\tArea: " << shape->area() << "\n"<< std::endl;
    }
}  
