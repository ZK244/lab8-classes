#include <iostream>
#include "Person.h"
using namespace std;

int main(){
  Person z = Person("Zaya");
  Person m = Person("Maryam");
  Person f = Person("Fabia");
  Person a = Person("Amal");

  cout<<"People: ";
  z.display();
  m.display();
  f.display();
  a.display();

  z.befriend(&m);
  z.befriend(&f);
  z.befriend(&a);
  m.befriend(&f);
  f.befriend(&a);
  a.befriend(&z);

  cout<<"\nBefriending: \n";
  z.display();
  m.display();
  f.display();
  a.display();

  z.unfriend(&f);
  cout<<"\n Unfriending: \n";
  z.display();
}
