#include <string>
#include <vector>
using namespace std;

class Person{
  string name;
  vector <Person*> friends;

 public:
  Person(string);
  void befriend(Person*);
  void unfriend(Person*);
  void display();
};
