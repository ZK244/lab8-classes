#include "CustomerCounter.h"
#include <iostream>
using namespace std;

CustomerCounter::CustomerCounter(int max){
  this->customer_count =0;
  this->max_customers=max;
};

void CustomerCounter::add(int num){
  if (this->customer_count + num > this->max_customers){
    this->customer_count = this->max_customers;
  }
  else{
    this->customer_count += num;
  }
}

void CustomerCounter::subtract(int num){
  if (this->customer_count - num < 0){
    this->customer_count = 0;
  }
  else{
    this->customer_count -= num;
  }
}

void CustomerCounter::display(){
  cout<< "Customer count is: " << this->customer_count << "\nMax customers: " << this->max_customers << std::endl;
}
